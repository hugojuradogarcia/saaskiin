<?php

    if (!isset($CFG))
        include_once '../config.php';
    include_once $CFG->datarootUti;
    include_once $CFG->datarootBOSaaskiin.'UsersBO.php';
    include_once $CFG->datarootBOSaaskiin.'SolicitudesBO.php';

    date_default_timezone_set("America/Mexico_City");
    $utilities = new Utilities();
    $users = new UsersBO();
    $solicitudes = new SolicitudesBO();
    $response = new stdClass();
    $namecontact = $utilities->getParam("namecontact", "");
    $email= $utilities->getParam("email", "");
    $product= $utilities->getParam("product", "");
    $telefono= $utilities->getParam("telefono", "");
    $mensaje = $utilities->getParam("mensaje", "");
    
    

    $getData = json_decode($posted_data);
    $creation_date = date("Y-m-d H:i:s");

    $filtro = " email='$email'";
    $contador = $users->getCountUsers($filtro);
    if($contador == 0){
        $data = array($namecontact , '', '', $email, $telefono, '', $creation_date);
        try {
            $id = $users->insertUsers($data);
            if($id > 0){
                $data2 = array($product,$id,$mensaje ,$creation_date);
                $id2 = $solicitudes->insertSolicitudes($data2);
                $response->reformando = ($id2 > 0) ? true : false;
            }else{
                $response->reformando = false;
            }            
        } catch (Exception $ex) {
            $response->reformando = false;
        }
    }else{
        $query = $users->getAllDataUsers($filtro);
        foreach($query as $datas){
            $id = $datas->id;
        }
        if($id > 0){
            $data2 = array($product,$id,$mensaje,$creation_date);
            $id2 = $solicitudes->insertSolicitudes($data2);
            $response->reformando = ($id2 > 0) ? true : false;
        }
    }
        
    echo json_encode($response);
    
?>
