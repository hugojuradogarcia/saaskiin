<?php
if (!isset($CFG))
        include_once 'config.php';
        include_once $CFG->datarootUti;
        include_once $CFG->datarootBOSaaskiin.'ProductsBO.php';
    date_default_timezone_set("America/Mexico_City");
    $utilities = new Utilities();
    $products = new ProductsBO();
    
    $response = new stdClass();
    $array = array();
    $dataInfo = $products->getAllDataProducts();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SáasKíin</title>
    <link href="http://saaskiin.com/img/favicon.png" type="image/png" rel="icon">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,900italic">
    <link id="jquery.fancybox-css" media="all" type="text/css" href="css/jquery.fancybox.css" rel="stylesheet">
    <link id="kadence_bootstrap-css" media="all" type="text/css" href="css/bootstrap.css" rel="stylesheet">
    <link id="kadence_theme-css" media="all" type="text/css" href="css/virtue.css" rel="stylesheet">
    <link id="virtue_skin-css" media="all" type="text/css" href="css/default.css" rel="stylesheet">
    <link id="roots_child-css" media="all" type="text/css" href="css/style.css" rel="stylesheet">
    <link id="redux-google-fonts-css" media="all" type="text/css" href="https://fonts.googleapis.com/css?family=Pacifico%3A400%7CLato%3A400%2C700%7CGeorgia%3A700%2C400&subset=latin&ver=1422151648" rel="stylesheet">
    <!--<link rel="stylesheet" href="css/autoptimize_74cf5543c20f7104268b898327233b01.css" media="all" type="text/css">-->

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="http://saaskiin.com/js/autoptimize_2224ae0bd693af2c10779433ca32554a.js" defer="" type="text/javascript"></script>
    <script src="lib/js/vendor/jquery-ui.min.js" type="text/javascript"></script>
    <script src="lib/js/vendor/angular.min.js" type="text/javascript"></script>
    <script src="lib/js/custom/controllerforms.js" type="text/javascript"></script>
    <script src="lib/js/custom/functions-reusable.js" type="text/javascript"></script>
    <!--<script src="lib/js/custom/utilities.js" type="text/javascript"></script>-->
    <script src="lib/js/custom/captcha.js" type="text/javascript"></script>
    <script src="lib/js/custom/muestreo.js" type="text/javascript"></script>
    
    
</head>

<body data-ng-app="ControllerMain">
    <div id="wrapper" class="container">

        <?php include_once("inc/header.php"); ?>
        <div class="wrap contentclass" role="document">
            <div id="pageheader" class="titleclass">
                <div class="container"></div>
            </div>
            <div id="content" class="container">
                <div class="page-header">
                    <h1 class="page-title">Contacto</h1>
                </div>
                <div class="container">
                    <div class="row clearfix">
                        <div class="col-md-1 column">
                        </div>
                        <div class="col-md-5 column" data-ng-controller="ControllerForm">
                            
                            <form name="FormControllerAction" id="FormControllerAction"  role="form" novalidate>
                                
                                <small class="errorSpan" data-ng-show="FormControllerAction.namecontact.$dirty && FormControllerAction.namecontact.$error.required || submitted && FormControllerAction.namecontact.$error.required">El nombre es requerido.</small>
                                <small class="errorSpan" data-ng-show="FormControllerAction.namecontact.$error.pattern">El nombre debe ser mayor a 3 digitos y solo debe contener letras.</small>
                                <input type="text" autocomplete="off" placeholder='Nombre' name='namecontact' data-ng-model='formData.namecontact' required ng-pattern="soloLetras" data-ng-class='{ error: FormControllerAction.namecontact.$invalid && FormControllerAction.namecontact.$dirty, warning: FormControllerAction.namecontact.$error.namecontact , success: FormControllerAction.namecontact.$valid }'>
                                <br><br>
    							<span class="errorSpan" data-ng-show='FormControllerAction.email.$dirty && FormControllerAction.email.$error.required || submitted && FormControllerAction.email.$error.required'>El email es requerido</span>
    							<span class="errorSpan" data-ng-show='FormControllerAction.email.$error.pattern'>Por favor coloca un email correcto.</span>
                                <input type="text" autocomplete="off" placeholder='Correo electronico' name='email' data-ng-model='formData.email' required ng-pattern="correovalido" data-ng-class='{ error: FormControllerAction.email.$invalid && FormControllerAction.email.$dirty, warning: FormControllerAction.email.$error.email, success: FormControllerAction.email.$valid }'>
                                <br><br>
                                <span class="errorSpan" data-ng-show="FormControllerAction.product.$dirty && FormControllerAction.product.$error.required || submitted && FormControllerAction.product.$error.required">El producto es requerido.</span>
                                <select id="product" name="product" ng-model="formData.product"  required data-ng-class='{ error: FormControllerAction.product.$invalid && FormControllerAction.product.$dirty , success: FormControllerAction.product.$valid }'>
    								<option value="">Selecciona un producto</option>
                                    <?php
                                        if(count($dataInfo) > 0){
                                            foreach($dataInfo AS $viewInfo){
                                                echo "<option value='$viewInfo->id'>$viewInfo->nombre_producto</option>";                     
                                            }
                                        }
                                    ?>
    							</select>    							
                                <br><br>
                                <span class="errorSpan" data-ng-show='FormControllerAction.telefono.$dirty && FormControllerAction.telefono.$error.required || submitted && FormControllerAction.telefono.$error.required'>El teléfono es requerido</span>
                                <span class="errorSpan" data-ng-show='FormControllerAction.telefono.$error.pattern'>Por favor coloca un teléfono correcto.</span>
                                <input type="text" maxlength="10" required placeholder='Teléfono' name='telefono' id="telefono" data-ng-model='formData.telefono' ng-pattern="dieznumeros" data-ng-class="{ error: FormControllerAction.telefono.$invalid && FormControllerAction.telefono.$dirty , success: FormControllerAction.telefono.$valid }">
        							
                                <br><br>
                                
                                <textarea name="mensaje" rows="4" id="mensaje" placeholder="mensaje" tabindex="6" style="width: 70%; height: 106px;" data-ng-model='formData.mensaje'></textarea>
                                <br>
                                <br>
                                 
				<div class="captcha-resultado">
				<span class="errorSpan" data-ng-show="FormControllerAction.resultado.$dirty && FormControllerAction.resultado.$error.required || submitted && FormControllerAction.resultado.$error.required">Debes colocar un resultado.</span>
				<span class="errorSpan" data-ng-show=''>Por favor colocar el resultado correcto.</span>
				<span class="newsize">Demuestra que no eres un robot</span>
				<div class="lineachi">
				<captcha field1="{{field1}}" operator="{{operator}}" field2="{{field2}}"></captcha>
				</div>
				
				<div>
				<input name="resultado" id="resultado" data-ng-model="formData.resultado" data-wc-unique="formData.resultado" required />
				</div>
				</div>
							
                                <br />
                                <br />
                                
                                
                                <button id="yesyes" class="button" data-ng-click="sendDataForm(FormControllerAction)">Enviar</button>
                                
                            </form>
                        </div>
                        <div class="col-md-6 column">
                            <img class="img-circle" alt="140x140" src="http://saaskiin.com//img/contact.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once("inc/footer.php"); ?>

    </div>
</body>

</html>
