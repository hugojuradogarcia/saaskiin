<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SáasKíin</title>
    <link href="http://saaskiin.com/img/favicon.png" type="image/png" rel="icon">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,900italic">
    <link id="jquery.fancybox-css" media="all" type="text/css" href="css/jquery.fancybox.css" rel="stylesheet">
    <link id="kadence_bootstrap-css" media="all" type="text/css" href="css/bootstrap.css" rel="stylesheet">
    <link id="kadence_theme-css" media="all" type="text/css" href="css/virtue.css" rel="stylesheet">
    <link id="virtue_skin-css" media="all" type="text/css" href="css/default.css" rel="stylesheet">
    <link id="roots_child-css" media="all" type="text/css" href="css/style.css" rel="stylesheet">
    <link id="redux-google-fonts-css" media="all" type="text/css" href="https://fonts.googleapis.com/css?family=Pacifico%3A400%7CLato%3A400%2C700%7CGeorgia%3A700%2C400&subset=latin&ver=1422151648" rel="stylesheet">
    <!--<link rel="stylesheet" href="css/autoptimize_74cf5543c20f7104268b898327233b01.css" media="all" type="text/css">-->

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="http://saaskiin.com/js/autoptimize_2224ae0bd693af2c10779433ca32554a.js" defer="" type="text/javascript"></script>

</head>

<body>
    <div id="wrapper" class="container">

        <?php include_once("inc/header.php"); ?>
        <div class="wrap contentclass" role="document">
            <div id="pageheader" class="titleclass">
                <div class="container"></div>
            </div>
            <div id="content" class="container">
                <div class="row">
                    <div class="main col-md-12" role="main">
                        <h1>AVISO DE PRIVACIDAD DEL SITIO  www.saaskiin.com</h1>
                        <p>En cumplimiento de lo dispuesto por la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, (en adelante LFPDPPP), su Reglamento, los lineamientos del Aviso de Privacidad y demás disposiciones aplicables, Saas Kiin (en adelante  “SK”) desea hacer del conocimiento del usuario del sitio web www.saaskiin.com su aviso de privacidad respecto del tratamiento y protección de los datos de carácter personal de aquellas personas que voluntariamente se comunican a través del correo electrónico con SK, llenan formularios en que se recaban datos personales o que ingresan y/o utilizan cualquier otro servicio ofrecido en el sitio web si es que esto implica la comunicación de sus datos personales.</p>
                        <p>En este aviso de privacidad se contienen todos los aspectos relacionados con el tratamiento de datos personales que SK lleva a cabo como responsable del mismo a través del sitio web. Si el usuario (tras la lectura del presente documento) continúa utilizando el sitio web, estará manifestando su aceptación plena y expresa de este aviso de privacidad y la manera en que será el tratamiento de sus datos personales. En caso contrario el usuario deberá salir del sitio web.</p>
                        <p>Se informa al usuario que cualquier tratamiento de datos personales, quedará bajo el ámbito de aplicación de la legislación vigente en México en materia de protección de datos, establecida por la LFPDPPP y su normativa.</p>
                        <p>Se entenderá por dato de carácter personal, cualquier información concerniente a personas físicas identificadas o identificables (incluyendo la dirección del correo electrónico y/o la dirección IP en el caso en que identifiquen inequívocamente a los interesados) y por usuario, cualquier persona física identificada o identificable que comunique sus datos de carácter personal a través de este sitio y/o los servicios disponibles en el mismo tal como (sin que esta enumeración se entienda de carácter limitativo), correo electrónico, llenado de formularios por los que se recaban datos personales, participación en campañas y promociones, creación de perfiles, uso de chats y foros, publicación de imágenes y otros contenidos, y en general uso de cualquier servicio presente en el sitio web que implique la comunicación de sus datos personales.</p>
                        <h3>A) Identidad DEL RESPONSABLE</h3>
                        <p>SK informa al usuario del sitio web de la existencia de varios tratamientos y bases de datos personales cuyo responsable es “Saas Kiin”.</p>
                        <h3>B) Datos Personales, FINALIDADES DEL TRATAMIENTO DE DATOS Y CONSENTIMIENTO DEL USUARIO</h3>
                        <p>El usuario que accede al sitio web de SK no está obligado a proporcionar información personal para la visualización del sitio web, por tanto, cualquier comunicación de datos a tales efectos ocurrirá porque el usuario voluntariamente ha decidido una navegación o uso personalizado de los servicios.</p>
                        <p>Los datos personales que pueden recabarse durante la navegación del usuario en el sitio son: nombre completo, dirección, código postal, correo electrónico, ciudad, estado, teléfono.</p>
                        <p>Cuando el usuario no se haya registrado en este sitio web y lleve a cabo una navegación anónima, debe tener en cuenta y estar informado que el envío de un correo electrónico a SK, así como la comunicación por el usuario a esta entidad de cualesquiera otros datos personales a través de cualquier medio, conlleva o implica su consentimiento libre, inequívoco, específico, informado y expreso para el tratamiento de los datos personales proporcionados por la citada compañía.</p>
                        <p>En caso de que el usuario contacte a SK para realizar consultas o solicitar información, dicho tratamiento de datos se realizará con la finalidad de atender y contestar las comunicaciones recibidas, así como enviar la información solicitada a la citada entidad.</p>
                        <p>En consecuencia, si el usuario no está de acuerdo con el tratamiento de sus datos por SK, deberá abstenerse de comunicar o enviar sus datos personales a esta compañía a través de cualquier medio, tal como correo electrónico o llenado de formularios, efectuando una navegación anónima por el sitio web.</p>
                        <p>Sus datos personales sólo serán utilizados para propósitos limitados, tal como los expuestos en el presente y/o aquellos que en su caso sean informados inequívocamente de forma previa a recabar y tratar los datos personales del usuario.</p>
                        <h2>Comunicaciones para el Usuario</h2>
                        <p>Con el fin de que SK pueda gestionar correctamente su relación con el usuario, SK podrá enviarle comunicaciones electrónicas. Estas comunicaciones serán (i) las necesarias para la entrega de su producto; (ii) mensajes necesariamente vinculados a contestar su solicitud de cotizacion. (iii) envío de promociones.</p>
                        <h3>C) OTROS DESTINATARIOS DE La INFORMACIÓN</h3>
                        <p>SK advierte al usuario que sólo es responsable y garantiza la confidencialidad, seguridad y tratamiento de los datos conforme al presente aviso, respecto de los datos de carácter personal que recabe directamente del usuario a través del presente sitio web no teniendo ningún tipo de responsabilidad respecto de los tratamientos y posteriores utilizaciones de los datos personales que pudieran efectuarse por terceros prestadores de servicios que tuvieren acceso a la información del usuario por motivo de la relación que en este acto el usuario establece con SK en términos de las transferencias que en este aviso de privacidad el usuario ha sido noticiado y acepta que SK puede realizar.</p>
                        <p>Por terceros prestadores de servicios de la sociedad de la información se entenderán, sin carácter limitativo, aquellas personas físicas o jurídicas que presten los siguientes servicios: (i) Transmisión por una red de comunicación de datos facilitados por los destinatarios del servicio. (ii) Servicios de acceso a la citada red. (iii) Servicios de almacenamiento o alojamiento de datos. (iv) Suministro de contenidos o información.</p>
                        <p>Asimismo, SK no se hace responsable de los tratamientos de datos que lleven a cabo los terceros que tengan hipervínculos en el sitio web de  SK ni de aquellos responsables a quienes a través de hipervínculos SK remite a los usuarios de su sitio web.</p>
                        <h3>D) SÍGUENOS EN REDES SOCIALES</h3>
                        <p>SK hace del conocimiento del usuario que está o puede estar presente en redes sociales, como Facebook y Twitter. El tratamiento de los datos de los usuarios que se hagan seguidores de las páginas oficiales de SK se regirá por las condiciones previstas en los términos y condiciones de la red social que corresponda en virtud de que cualquier dato que la red social requiera sea recabado es responsabilidad de esta y no de SK por lo que se recomienda el acceso a la política de privacidad y condiciones de uso de las mencionadas páginas o en su caso, de la propia red social, con el fin de conocer la información derivada del tratamiento de los datos de carácter personal y especialmente las condiciones y finalidades a las que serán destinados los datos que forman parte del perfil del usuario.</p>
                        <p>Respecto de los servicios de red social en los que SK ha creado o llegue a crear su propia página oficial, se ha previsto en el sitio web www.saaskiin.com la posibilidad de que el usuario marque sobre el icono de la red social y directamente acceda a la página oficial creada por SK haciéndose seguidor o fan de la misma. SK es responsable de la administración de dichas páginas siempre y cuando sean páginas oficiales y por tanto hayan sido creadas por SK. No obstante SK no se responsabiliza de las páginas no oficiales que terceros usuarios creen en las redes sociales.</p>
                        <p>SK se reserva el derecho de crear, editar, modificar y/o eliminar la página oficial sin necesidad de informar con carácter previo al usuario.</p>
                        <p>
                            Para realizar cualquier consulta en relación con el tratamiento de datos que realiza SK, el usuario podrá ponerse en contacto con SK en la dirección que se muestra a continuación 
                            <em>
                                <a href="contacto@saaskiin.com">contacto@saaskiin.com</a>
                            </em>
                        </p>
                        <h3>E) EJERCICIO DE LOS DERECHOS DE RECTIFICACIÓN, Cancelación Y oposición. limitación al uso o divulgación de los datos y revocación deL consentimiento</h3>
                        <p>El usurario tiene el derecho de rectificar sus datos en caso de ser inexactos o incompletos; solicitar sean cancelados cuando considere que son innecesarios para las finalidades para las cuales se recabaron y que no están siendo tratados conforme a los principios y deberes que establece la LFPDPPP, así como a oponerse a su tratamiento para fines específicos. De igual forma, el usuario podrá solicitar se limite el uso o divulgación de sus datos personales así como revocar el consentimiento que en su caso haya otorgado.</p>
                        <p>Asimismo, el usuario podrá dirigir su oposición mediante la remisión de un correo electrónico a la dirección: contacto@saaskiin.com, con el asunto “OPOSICIÓN”, así como derecho a revocar su consentimiento solicitando la baja del servicio de recepción de comunicaciones comerciales por correo electrónico mediante remisión de un correo electrónico a la dirección: contacto@saaskiin.com, con el asunto "BAJA".</p>
                        <h3>F) "COOKIES" Y "BEACONS"</h3>
                        <p>Para asegurar que el sitio web está siendo bien administrado y facilitar una mejor navegación dentro del mismo, SK, como el/los proveedor(es) de servicios de web podrán estar utilizando "cookies" (breves archivos de texto almacenados en el navegador del usuario) o "web beacons" (imágenes electrónicas que permiten al sitio Web contar el número de visitantes y usuarios que han ingresado a un sitio web en particular y acceder a ciertas "cookies") para almacenar y agregar información. SK podrá usar estas herramientas para rastrear información en sus sistemas e identificar categorías de usuarios del sitio web por puntos como direcciones de IP, dominio, tipo de navegador y páginas visitadas. Esta información se reporta al Webmaster de SK, y podrá ser utilizada para analizar el número de visitantes y usuarios de las diferentes áreas del sitio y asegurar con ello la utilidad de nuestro sitio como fuente de información útil y efectiva.</p>
                        <p>Tanto las "cookies" como los "web beacons" almacenan información personal tal como nombres o direcciones electrónicas. La mayoría de navegadores permiten a los usuarios rechazar las "cookies" (generalmente en el menú herramientas). Cabe mencionar que en circunstancias específicas, el acceso puede ser denegado en ciertas partes de nuestro sitio a aquellos visitantes y usuarios cuyos navegadores no permitan el uso de "cookies".</p>
                        <h3>G) MEDIDAS DE SEGURIDAD ADOPTADAS </h3>
                        <p>SK informa al usuario que, de conformidad con lo dispuesto en la LFPDPPP y su Reglamento, ha adoptado las medidas de índole técnica, administrativa y físicas necesarias para garantizar la seguridad de los datos de carácter personal y evitar la alteración, pérdida, tratamiento o acceso no autorizado, habida cuenta del estado de la tecnología, la naturaleza de los datos almacenados y los riesgos a que están expuestos, ya provengan de la acción humana o del medio físico o natural y que sólo registrará datos de carácter personal en bases de datos que reúnan las condiciones que se determinan en los mencionados ordenamientos con respecto a su integridad y seguridad y a las de los centros de tratamiento, locales, equipos, sistemas y programas.</p>
                        <h3>H) RECOMENDACIONES A LOS USUARIOS</h3>
                        <p>Se recomienda a los usuarios que utilicen las últimas versiones de los programas informáticos para su navegación por Internet dada la incorporación en estos de mayores medidas de seguridad.</p>
                        <p>Igualmente se recomienda a los usuarios utilizar los mecanismos de seguridad que tengan a su alcance (servidores web seguros, criptografía, firma digital, firewall, etc.) para proteger la confidencialidad e integridad de sus datos en la medida en que le resulte necesario, dado que existen riesgos de suplantación de la personalidad o violación de la comunicación.</p>
                        <p>Se advierte a los usuarios que tengan presente que, salvo que empleen mecanismos de cifrado, el correo electrónico en Internet no es seguro. Los mensajes de correo y los foros de discusión pueden ser objeto de falsificación y suplantación de personalidad, lo que debe tenerse en cuenta siempre que se usen. Si el usuario no quiere publicar su dirección de correo electrónico, deberá configurar su navegador para que no deje su dirección de correo en los servidores web a los que accede.</p>
                        <h3>I) ACTUALIZACIÓN DEL AVISO DE PRIVACIDAD</h3>
                        <p>Ocasionalmente, SK actualizará este aviso de privacidad y tratamiento de datos personales de los usuarios del sitio web.</p>
                        <p>Cualquier modificación de este aviso será publicada y advertida en este sitio web y en el aviso mismo, teniendo en cuenta el usuario que, el tratamiento de los datos que hubiera comunicado a SK se regirá por las políticas y condiciones del tratamiento de los datos vigentes y publicadas en el momento en que haya proporcionado sus datos personales a las citadas entidades sin perjuicio de la aplicación preferente de los textos legales específicos al efecto tal como textos informativos insertos en cupones y formularios de recogida de datos, condiciones de servicios particulares.</p>
                        <p>En todo caso será responsabilidad del usuario acceder periódicamente al aviso de privacidad publicado en el sitio web a fin de conocer en todo momento la última versión.</p>
                        <p>Se recuerda al usuario que, el acceso a este sitio web, el registro en el sitio y/o el uso de sus servicios conlleva la aceptación expresa del usuario de este aviso de privacidad y tratamiento de datos personales, siendo su responsabilidad la lectura del mismo. En caso de que el usuario no esté de acuerdo deberá abstenerse de navegar por este sitio web, registrarse y/o utilizar los servicios y contenidos del sitio de forma que no se comuniquen sus datos personales a SK realizando una navegación anónima.</p>
                        <h3>J) CONTACTO</h3>
                        <p>
                            Se informa al usuario que se recibirá con agradecimiento los comentarios que el usuario le pueda proporcionar en relación con este aviso de privacidad y protección de datos personales. A tales efectos o para realizar cualquier consulta en relación con la misma, el usuario podrá ponerse en contacto con SK tanto por medios electrónicos como mediante correo postal en la dirección que se muestra a continuación:
                            <em>
                                <a href="contacto@saaskiin.com">contacto@saaskiin.com</a>
                            </em>
                        </p>
                    </div>
                </div>
            </div>
        </div>

       <?php include_once("inc/footer.php"); ?>
    </div>
</body>

</html>
