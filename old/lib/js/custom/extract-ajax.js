
/**
* nameElement -> nombre del elemento
* nameFunction -> nombre de la funcion maestra
*/

function SelectBuildAutoLoad(nameElement,nameFunction,getReference,getTax){
	jQuery.post(MyAjax.url, {
		nonce : MyAjax.nonce,
		action : nameFunction,
		referencia : getReference,
		taxonomy : getTax
	}, function(response) {
		if(response.result){
			var formatArray = response.build;
			createItems(formatArray,nameElement);
		}
	});	
}

function createItems(getArray,getElement){
	for(i=0; i < getArray.length; i++){
		$('#'+getElement).append("<option value='"+getArray[i].slug+"'>"+getArray[i].name+"</option>");
	}
}

function ChangeSelectSub(){
	$("#estado").change(function(e) {
		var id = $("#estado").find(':selected').val();
		$("#sucursal").empty();
		jQuery.post(MyAjax.url, {
			nonce : MyAjax.nonce,
			action : 'llenaselectsub' ,
			referencia : id
		}, function(response) {			
			if(response.result){
				var formatArray = response.build;
				$('#sucursal').append("<option value='Call Center'>Call Center</option>");
				createItems(formatArray,"sucursal");
			}else{
				$('#sucursal').append("<option value='Call Center'>Call Center</option>");
			}
		});
	});
}



function asignValue(element,data){
	$("#"+element).val(data);
}


function SelectBuildAutoLoadIntern(nameElement,nameAsign,nameFunction,getTax){
	$("#"+nameElement).change(function(e) {
		var id = $("#"+nameElement).find(':selected').val();
		
		
		$("#"+nameAsign).empty();
		jQuery.post(MyAjax.url, {
			nonce : MyAjax.nonce,
			action : nameFunction ,
			referencia : id,
			taxonomy : getTax
		}, function(response) {
			console.log(response);
			if(response.result){
				var formatArray = response.build;
				//$('#'+nameAsign).append("<option value='Call Center'>Call Center</option>");
				createItems(formatArray,nameAsign);
			}else{
				$('#sucursal').append("<option value=''>Selecciona una opcion</option>");
			}
		});
	});
	
	
}