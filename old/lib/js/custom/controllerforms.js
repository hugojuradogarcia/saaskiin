//var baseurlmaster = 'http://10.20.30.88/webservice.php';
var baseurlmaster = 'ajxRsp/regSendData.php';
var myEl = "";
var newIDContacto = "";
var graciasuniversal = "thank_you_page.php";

var app = angular.module('ControllerMain', ["udpCaptcha"]);
app.controller('ControllerForm', ['$scope','$http','$location','genericServices','getchallengeMaster','sendMailProccess','$captcha', function($scope,$http,$location,genericServices,getchallengeMaster,sendMailProccess,$captcha){

	$scope.formData = {};

	// Expresiones regulares
	$scope.soloLetras = /^([a-zA-Z ñáéíóú\s]){3,}$/;
	$scope.correovalido = /^\w+([\.-.]?\w+)*@\w+([\.-.]?\w+)*(\.\w{2,4})+$/;
	$scope.dieznumeros = /^([0-9]){10,10}$/;

	
	
	$scope.addClass = function(element,className){
		myEl = angular.element( document.querySelector( '#'+element ) );
		myEl.addClass(className);
	}

	$scope.quitaClass = function(element,className){
		myEl = angular.element( document.querySelector( '#'+element ) );
		myEl.removeClass(className);
	}
	

	
	
	$scope.checkboxselected = function (object) {
		return object;
  	}

	// Send data post form
	$scope.sendDataForm = function(formData){
		
		$scope.submitted = true;
		
		if (formData.$invalid) {
            return;
        }



		
		var message = ($scope.formData.mensaje != undefined) ? $scope.formData.mensaje : "";
		var formdata = {
			"name" : $scope.formData.namecontact,
			"email" : $scope.formData.email,
			"product" : $scope.formData.product,
			"telefono" : $scope.formData.telefono,
			"mensaje" : message
		};
		jQuery.get( baseurlmaster+'?'+$("#FormControllerAction").serialize(), function(response){

			if(response.reformando){
				window.location.href=graciasuniversal;
			}else{
				console.log("Ocurrio un error");
			}
		},"json");
		

	}
}]);
