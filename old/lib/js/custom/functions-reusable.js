var d = new Date();
var n = d.getFullYear();

app.config(['$locationProvider', function($locationProvider) {
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});
}]);

/*Llamadas WS*/
app.service('getchallengeMaster', function($http){
    delete $http.defaults.headers.common['X-Requested-With'];
    this.getData = function(callbackFunc) {
        $http({
            method: 'GET',
            url: 'http://crm.mundojoven.com:8080/webservice.php',
			//url : 'http://10.20.30.88/webservice.php',
            params: {'operation':'getchallenge', 'username':'liveuser'},
        }).success(function(data){
            callbackFunc(data);
        }).error(function(){
            console.log("error al comunicarse al servicio");
            window.location.href=errorpage;
        });
    }
});

app.factory("genericServices", function() {
	return {
		doSomething: function() {
			var estampa = [
		    {
				id:1, name : "Aguascalientes",
				sucursales:[
		      		{name : "Call Center" },
		      		{name : "Aguascalientes" }]
		    },
		    {
				id : 2, name : "Baja California",
				sucursales:[
					{name : "Call Center" },
					{name : "Ensenada"}]
			},
		    {
		    	id : 3, name : "Chiapas",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Tuxtla Gutiérrez"}]
		  	},
		  	{
		    	id : 4, name : "Coahuila",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Torreon"},
		      		{name : "Saltillo"}]
		  	},
		  	{
		    	id : 5, name : "Distrito Federal",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Aeropuerto"},
		      		{name : "Bosque de las Lomas"},
		      		{name : "Coapa"},
		      		{name : "Insurgentes Sur"},
		      		{name : "Lindavista"},
		      		{name : "Polanco"},
		      		{name : "Revolución"},
		      		{name : "San Jerónimo"},
		      		{name : "Zócalo"},
		      		{name : "UNAM"}]
		  	},
		  	{
		    	id : 6, name : "Estado de México",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Interlomas"},
		      		{name : "Satélite"},
		      		{name : "Toluca"}]
		  	},
		  	{
		    	id : 7, name : "Guanajuato",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "León"}]
		  	},
		  	{
		    	id : 8, name : "Jalisco",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Guadalajara Vallarta"},
		      		{name : "Guadalajara Plaza Ciudadela"}]
		  	},
		  	{
		    	id : 9, name : "Michoacán",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Morelia"}]
		  	},
		  	{
		    	id : 10, name : "Morelos",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Cuernavaca"}]
		  	},
		  	{
		    	id : 11, name : "Nuevo León",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Monterrey Tecnológico"},
		      		{name : "Monterrey Valle"},
		      		{name : "Monterrey TEC Campus MTY"},
		      		{name : "Monterrey Cumbres"}]
		  	},
		  	{
		    	id : 12, name : "Oaxaca",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Oaxaca"}]
		  	},
		  	{
		    	id : 13, name : "Puebla",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Puebla La Paz"},
		      		{name : "Puebla Plaza W"}]
		  	},
		  	{
		    	id : 14, name : "Querétaro",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Querétaro"}]
		  	},
		  	{
		    	id : 15, name : "Quintana Roo",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Cancún"}]
		  	},
		  	{
		    	id : 16, name : "San Luis Potosí",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "San Luis Potosí"}]
		  	},
		  	{
		    	id : 17, name : "Tabasco",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Villahermosa"}]
		  	},
		  	{
		    	id : 18, name : "Tamaulipas",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Cd. Victoria"}]
		  	},
		  	{
		    	id : 19, name : "Veracruz",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Orizaba"},
		      		{name : "Xalapa"}]
		  	},
		  	{
		    	id : 20, name : "Yucatán",
		    	sucursales:[
		      		{name : "Call Center"},
		      		{name : "Mérida"}]
		  	}

		  	] ;
			return estampa;
		},
		levelsLanguage: function() {
			var levels = [
		    { nivel : "Basico" },
		    { nivel : "Medio" },
		    { nivel : "Avanzado" }

		  	] ;
			return levels;
		},
		degreeInterest: function() {
			var interests = [
		    { interest : "Poco" },
		    { interest : "Medio" },
		    { interest : "Mucho" },
		    { interest : "Demasiado" }

		  	] ;
			return interests;
		},
		programInterest: function() {
			var programs = [
		    { programname : "Certificaciones" },
		    { programname : "Licenciaturas" },
		    { programname : "Diplomado" },
		    { programname : "Posgrado" },
		    { programname : "Maestrias" }

		  	] ;
			return programs;
		},
		durationWeeks: function() {
			var weeks = [
		    { durweek : "1 Semana" },
		    { durweek : "2 Semanas" },
		    { durweek : "3 Semanas" },
		    { durweek : "4 Semanas" },
		    { durweek : "5 Semanas" },
		    { durweek : "6 Semanas" }

		  	] ;
			return weeks;
		},
		relationMonth: function() {
			var month = [
			    { monthsel : "Enero", itemmont : "01" },
			    { monthsel : "Febrero", itemmont : "02" },
			    { monthsel : "Marzo", itemmont : "03" },
			    { monthsel : "Abril", itemmont : "04" },
			    { monthsel : "Mayo", itemmont : "05" },
			    { monthsel : "Junio", itemmont : "06" },
			    { monthsel : "Julio" , itemmont : "07"},
			    { monthsel : "Agosto", itemmont : "08" },
			    { monthsel : "Septiembre", itemmont : "09" },
			    { monthsel : "Octubre", itemmont : "10" },
			    { monthsel : "Noviembre", itemmont : "11" },
			    { monthsel : "Diciembre", itemmont : "12" }
		  	] ;
			return month;
		},
		relationTypeProgram: function(){
			var relationComp = [
		    {
				id:1, name : "Idiomas",
				sucursales:[
		      		{duration : "1 Semana" , id : 1},
		      		{duration : "2 Semanas" , id : 2},
		      		{duration : "3 Semanas" , id : 3},
		      		{duration : "4 Semanas" , id : 4},
		      		{duration : "5 Semanas" , id : 5},
		      		{duration : "6 Semanas" , id : 6}
	      		]
		    },
		    {
				id:2, name : "Campamentos de Verano/Invierno",
				sucursales:[
		      		{duration : "Junio " + n , id : 1},
		      		{duration : "Julio " + n , id : 2}
	      		]
		    },
		    {
				id:3, name : "Works",
				sucursales:[
		      		{duration : "1 Semana" , id : 1},
		      		{duration : "2 Semanas", id : 2},
		      		{duration : "3 Semanas", id : 3},
		      		{duration : "4 Semanas", id : 4},
		      		{duration : "5 Semanas", id : 5},
		      		{duration : "6 Semanas", id : 6}
	      		]
		    },
		    {
				id:4, name : "Voluntariados",
				sucursales:[
		      		{duration : "1 Semana" , id : 1 },
		      		{duration : "2 Semanas" , id : 2}
	      		]
		    }
		    ];
			return relationComp;


		},
		relationProducts: function(){
			var relationProd = [
		    {
		    	id : 1,
		    	name : "Vuelos",
		    	value : "vuelos"
		    },
		    {
		    	id : 2,
		    	name : "Seguro de viaje",
		    	value : "seguro"
		    },
		    {
		    	id : 3,
		    	name : "ISIC",
		    	value : "isic"
		    },
		    {
		    	id : 4,
		    	name : "Guia de destino",
		    	value : "guia"
		    },
		    {
		    	id : 5,
		    	name : "Trenes",
		    	value : "trenes"
		    }

		    ];

		    return relationProd;
		}
	}
});

app.directive('checkList', function() {
  return {
    scope: {
      list: '=checkList',
      value: '@'
    },
    link: function(scope, elem, attrs) {
      var handler = function(setup) {
        var checked = elem.prop('checked');
        var index = scope.list.indexOf(scope.value);

        if (checked && index == -1) {
          if (setup) elem.prop('checked', false);
          else scope.list.push(scope.value);
        } else if (!checked && index != -1) {
          if (setup) elem.prop('checked', true);
          else scope.list.splice(index, 1);
        }
      };

      var setupHandler = handler.bind(null, true);
      var changeHandler = handler.bind(null, false);

      elem.on('change', function() {
        scope.$apply(changeHandler);
      });
      scope.$watch('list', setupHandler, true);
    }
  };
});




/*Llamadas WS*/
app.service('sendMailProccess', function($http){
    delete $http.defaults.headers.common['X-Requested-With'];
	this.sayHello = function(baseDir,getParams,getUrlDir,getEmailSend,idMessageService){
		jQuery.post( baseDir, getParams, function(data){
			if(data.success)
			{
				$http({
					method: 'GET',
					url: 'http://sofia.mundojoven.com:8090/ssbWsEp/rest/restExecutionService/msg/'+idMessageService+'/destiny=' + getEmailSend
				}).success(function(data){
					window.location.href = getUrlDir;
				}).error(function(){
					window.location.href = errorpage;
				});
			}else{
				console.log("Error: " + data.error.code + " " + data.error.message);
				window.location.href = errorpage;
			}
		});
	}
});


app.directive('jqdatepickerOnlymonday', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
			var dateToday = new Date();
			var arr = [0, 2, 3, 4, 5, 6];
            $(element).datepicker({
                dateFormat: 'yy-mm-dd',
				changeMonth: true,
				minDate: dateToday,
				beforeShowDay: function(date) {
					var day = date.getDay();
					return [(arr.indexOf(day) == -1)];
				},
                onSelect: function(date) {
                    ctrl.$setViewValue(date);
                    ctrl.$render();
                    scope.$apply();
                }
            });
        }
    };
});

app.directive('jqdatepickerBirthday', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            $(element).datepicker({
                dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				yearRange: "-100:+0",
                onSelect: function(date) {
                    ctrl.$setViewValue(date);
                    ctrl.$render();
                    scope.$apply();
                }
            });
        }
    };
});


app.directive('wcUnique', ['$captcha', function ($captcha) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, formCtrl) {
            element.bind('blur', function (e) {
                if (!formCtrl || !element.val()) return;
				var currentValue = element.val();
				
				if($captcha.checkResult(currentValue) == true){
					element.removeClass('error');
					element.addClass('success');
					scope.isDisabled = false;
					$("#yesyes").attr("disabled",false);
				}else{
					$captcha.getOperation();
					element.removeClass('success');
					element.addClass('error');
					$("#yesyes").attr("disabled",true);
					scope.isDisabled = true;
				}
            });
        }
    }
}]);