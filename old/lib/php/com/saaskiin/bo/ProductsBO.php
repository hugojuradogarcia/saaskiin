<?php

include_once $CFG->datarootDAOSaaskiin . 'ProductsDAO.php';

/**
 * @author Enginner system Edwin michael
 * @category Web developer system
 * @copyright 2015
 *
 * Description ProductsBO
 *
 */
class ProductsBO {

    private $productsDAO;

    public function __construct() {
        $this->productsDAO = new ProductsDAO();
    }

    public function __destruct() {
        unset($this->productsDAO);
    }

    private function validateArrayData() {
        return true;
    }

    public function getCountProducts($filter = null) {
        $count = $this->productsDAO->getCountProductsApp($filter);
        return $count;
    }

    public function getAllDataProducts($filter = null) {
        $resulset = null;
        $resulset = $this->productsDAO->getAllDataProductsApp($filter);
        return $resulset;
    }

    public function insertProducts($data) {
        $id = null;
        if ($this->validateArrayData()) {
            $id = $this->productsDAO->insertProductsApp($data);
        }
        return $id;
    }   

}
?>




