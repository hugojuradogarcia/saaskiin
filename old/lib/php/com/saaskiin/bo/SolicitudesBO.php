<?php

include_once $CFG->datarootDAOSaaskiin . 'SolicitudesDAO.php';

/**
 * @author Enginner system Edwin michael
 * @category Web developer system
 * @copyright 2015
 *
 * Description SolicitudesBO
 *
 */
class SolicitudesBO {

    private $solicitudesDAO;

    public function __construct() {
        $this->solicitudesDAO = new SolicitudesDAO();
    }

    public function __destruct() {
        unset($this->solicitudesDAO);
    }

    private function validateArrayData() {
        return true;
    }

    public function getCountSolicitudes($filter = null) {
        $count = $this->solicitudesDAO->getCountSolicitudesApp($filter);
        return $count;
    }

    public function getAllDataSolicitudes($filter = null) {
        $resulset = null;
        $resulset = $this->solicitudesDAO->getAllDataSolicitudesApp($filter);
        return $resulset;
    }

    public function insertSolicitudes($data) {
        $id = null;
        if ($this->validateArrayData()) {
            $id = $this->solicitudesDAO->insertSolicitudesApp($data);
        }
        return $id;
    }   

}
?>




