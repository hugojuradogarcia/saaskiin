<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SáasKíin</title>
    <link href="http://saaskiin.com/img/favicon.png" type="image/png" rel="icon">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,900italic">
    <link id="jquery.fancybox-css" media="all" type="text/css" href="css/jquery.fancybox.css" rel="stylesheet">
    <link id="kadence_bootstrap-css" media="all" type="text/css" href="css/bootstrap.css" rel="stylesheet">
    <link id="kadence_theme-css" media="all" type="text/css" href="css/virtue.css" rel="stylesheet">
    <link id="virtue_skin-css" media="all" type="text/css" href="css/default.css" rel="stylesheet">
    <link id="roots_child-css" media="all" type="text/css" href="css/style.css" rel="stylesheet">
    <link id="redux-google-fonts-css" media="all" type="text/css" href="https://fonts.googleapis.com/css?family=Pacifico%3A400%7CLato%3A400%2C700%7CGeorgia%3A700%2C400&subset=latin&ver=1422151648" rel="stylesheet">
    <!--<link rel="stylesheet" href="css/autoptimize_74cf5543c20f7104268b898327233b01.css" media="all" type="text/css">-->

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="http://saaskiin.com/js/autoptimize_2224ae0bd693af2c10779433ca32554a.js" defer="" type="text/javascript"></script>

</head>

<body>
    <div id="wrapper" class="container">

       <?php include_once("inc/header.php"); ?>

        <div class="wrap contentclass" role="document">
            <div id="pageheader" class="titleclass">
                <div class="container"></div>
            </div>
            <div id="content" class="container">
                <div class="row">
                    <div class="main col-md-12" role="main">
                        <h1>¿POR QUE SAAS KIIN?</h1>
                        <br />
                        <p>1.- Ayuda al fortalecimiento desde la raíz hasta las puntas previniendo su debilitamiento y la caída prematura.</p>
                        <p>2.- Es tan suave con tu cabello que podrás usarlo diariamente.</p>
                        <p>3.- Estimula el crecimiento de cabello. </p>
                        <p>4.- Cambios visibles en 3 meses de uso.</p>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once("inc/footer.php"); ?>

    </div>
</body>

</html>
