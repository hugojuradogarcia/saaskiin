<header>
    <nav role="navigation" class="navbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header hidden-web">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand hidden-web">
                <img src="img/logo3.fw.png" class="img-responsive img-logo">
            </a>
        </div>
        <!-- Collection of nav links and other content for toggling -->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <div class="col-md-2 col-md-offset-5 visible-md visible-lg inline">
                <img src="img/logo2.png" class="img-responsive img-full-width padding-top-bottom">
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a class="menu" href="store.php">Tienda</a></li>
                <li><a class="menu" href="why.php">¿Por qué Sáaskíin?</a></li>
                <li><a class="menu" href="contact.php">Contacto</a></li>
            </ul>
        </div>
    </nav>