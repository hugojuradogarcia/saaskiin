<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saaskiin</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>

</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <img src="img/logo.png" alt="SáasKíin" class="modifychange" />
                </div>
            </div>
        </div>
        <nav role="navigation" class="navbar navbar-mio">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header hidden-web">
                <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand hidden-web">Menu</a>
            </div>
            <!-- Collection of nav links and other content for toggling -->
            <div id="navbarCollapse" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a class="menu" href="#">Tienda</a></li>
                    <li><a class="menu" href="#">¿Por qué Sáaskíin?</a></li>
                    <li><a class="menu" href="#">Contacto</a></li>
                </ul>
            </div>
        </nav>
    </header>

    <footer>
        <div class="container">
            <div class="row">
                <div class="footercol col-md-4">
                    <h2>Temas de Interés</h2>
                    <ul class="alineadotemas quitapadding col-xs-12">
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="faq.php">Preguntas Frecuentes</a></li>
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="privacy.php">Aviso de Privacidad</a></li>
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="who.php">Quienes Somos</a></li>
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="policies.php">Políticas</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="footercol col-md-4">
                    <h2>Contacto</h2>
                    <ul class="alineadotemas quitapadding col-xs-12">
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="mailto:info@saaskiin.com">info@saaskiin.com</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="footercol col-md-4 text-center">
                    <h2>Redes Sociales</h2>
                    <ul class="alineadotemas2 quitapadding">
                        <li><a href="https://www.facebook.com/Shampoo-anticaida-SAAS-KIIN-1650813605134295/" target="_blank"><img width="63" height="63" title="Sáaskíin on Facebook" alt="saaskiin-facebook" src="http://saaskiin.com/img/facebook.png"></a></li>
                        <li><a href="https://twitter.com/saaskiin" target="_blank"><img width="64" height="64" title="Sáaskíin on Twitter" alt="saaskiin-twitter" src="http://saaskiin.com/img/twitter.png"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</body>

<script src="lib/js/vendor/jquery-1.11.3.min.js"></script>
<script src="lib/js/vendor/bootstrap.min.js"></script>
</html>