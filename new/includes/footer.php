<footer>
        <div class="container">
            <div class="row">
                <div class="footercol col-md-4">
                    <h2>Temas de Interés</h2>
                    <ul class="alineadotemas quitapadding col-xs-12">
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="faq.php">Preguntas Frecuentes</a></li>
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="privacy.php">Aviso de Privacidad</a></li>
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="who.php">Quienes Somos</a></li>
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="policies.php">Políticas</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="footercol col-md-4">
                    <h2>Contacto</h2>
                    <ul class="alineadotemas quitapadding col-xs-12">
                        <li class="col-xs-6 col-md-12 quitapadding"><a href="mailto:info@saaskiin.com">info@saaskiin.com</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="footercol col-md-4 text-center">
                    <h2>Redes Sociales</h2>
                    <ul class="alineadotemas2 quitapadding">
                        <li><a href="https://www.facebook.com/Shampoo-anticaida-SAAS-KIIN-1650813605134295/" target="_blank"><img width="63" height="63" title="Sáaskíin on Facebook" alt="saaskiin-facebook" src="img/facebook.png"></a></li>
                        <li><a href="https://twitter.com/saaskiin" target="_blank"><img width="64" height="64" title="Sáaskíin on Twitter" alt="saaskiin-twitter" src="img/twitter.png"></a></li>
                    </ul>
                </div>
                <div class="col-md-12 text-center">
                    <p class="copyright">© 2015 saaskiin.com. Todos los Derechos Reservados.</p>
                </div>
            </div>
        </div>
    </footer>