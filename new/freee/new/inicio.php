<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saaskinn</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main_style.css">
</head>
<body>
       <header>
       <div class="row">
        <div class="logotop col-md-12 text-center">
            <img src="img/logo.png" alt="Logo Saaskiin">
        </div>
       </div>

      <nav class="navbar navbar-saaskiin" role="navigation">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-menu">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand topmenu hidden-md hidden-lg" href="#">Menu</a>
            </div>

            <div class="collapse navbar-collapse" id="main-menu">
               <ul class="nav navbar-nav text-center">
                  <li class="active"><a class="" href="#">Tienda</a></li>
                  <li><a class="" href="#">¿Porqué Sáaskíin?</a></li>
                  <li><a class="" href="#">Contacto</a></li>
               </ul>
            </div> 
         </div>
      </nav>
   </header>

   <footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 block-footer-section">
                <h3>Temas de Interés</h3>
                <ul class="menu">
                    <li>
                        <a href="faq.php" class="link">Preguntas Frecuentes</a>
                    </li>
                    <li>
                        <a href="privacy.php" class="link">Aviso de Privacidad</a>
                    </li>
                    <li>
                        <a href="who.php" class="link">Quienes Somos</a>
                    </li>
                    <li>
                        <a href="policies.php" class="link">Políticas</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 block-footer-section">
                <h3>Contacto</h3>
                <p>
                    <a href="mailto:info@saaskiin.com">info@saaskiin.com</a>
                </p>
            </div>
            <div class="col-md-4 block-footer-section">
                <h3>Social</h3>
                <ul class="social">
                    <li>
                        <a href="https://www.facebook.com/pages/Shampoo-anticaida-SAAS-KIIN/1650813605134295" target="_blank"><img width="63" height="63" title="Sáaskíin on Facebook" alt="saaskiin-facebook" src="http://saaskiin.com/img/facebook.png"></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/saaskiin" target="_blank"><img width="64" height="64" title="Sáaskíin on Twitter" alt="saaskiin-twitter" src="http://saaskiin.com/img/twitter.png"></a>
                    </li>
                    </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="pull-right reservado">© 2015 saaskiin.com. Todos los Derechos Reservados.</p>
            </div>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="lib/js/vendor/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="lib/js/vendor/bootstrap.min.js"></script>
</html>