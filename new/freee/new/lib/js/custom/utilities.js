function toggleNavbarMethod() {
	if ($(window).width() > 768) {
		$('.navbar .dropdown').on('mouseover', function(){
			$('.dropdown-toggle', this).trigger('click'); 
		}).on('mouseout', function(){
			$('.dropdown-toggle', this).trigger('click').blur();
		});
	}else {
		addClass(".navbar-collapse","a.unique");
		addClass(".navbar-collapse","a.uniquedropdown");
	}
}

function addClass(classMain,identClassOrId){
	$(classMain + ' ' + identClassOrId).click(function(){
		$(".navbar-collapse").collapse('hide');
	});
}