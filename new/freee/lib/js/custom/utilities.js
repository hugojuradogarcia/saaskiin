var dateToday = new Date();
$(document).ready(function(){
	
$.datepicker.regional['es'] = {
 closeText: 'Cerrar',
 prevText: '<Ant',
 nextText: 'Sig>',
 currentText: 'Hoy',
 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
 weekHeader: 'Sm',
 dateFormat: 'dd/mm/yy',
 firstDay: 1,
 isRTL: false,
 showMonthAfterYear: false,
 yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);	
	
});

function esBisiesto(anio){
	return ((anio % 4 == 0 && anio % 100 != 0) || anio % 400 == 0) ? true : false;
}

function validaFecha(fecha) {
    var arreglo = fecha.split("-");
 
    if (arreglo.length != 3){
		return false;
	}
 
    if (!parseInt(arreglo[0]) || !parseInt(arreglo[1]) || !parseInt(arreglo[2])){
		return false;
	}
 
    var dia = parseInt(arreglo[2]);
    var mes = parseInt(arreglo[1]);
    var anio = parseInt(arreglo[0]);
 
    if (dia < 1 || dia > 31 || mes < 1 || mes > 12 || anio < 1){
		return false;
	}
 
    switch (mes) {
        case 4:
        case 6:
        case 9:
        case 11:
            if (dia > 30)
                return false;
            break;
        case 2:
            if (esBisiesto(anio)) {
                if (dia > 29)
                    return false;
            }
            else {
                if (dia > 28)
                    return false;
            }
    }
    return true;
}

function validaIntervalo(intervalo) {
    var primero = intervalo.substring(0, 1);
    var valor = intervalo.substring(1, intervalo.length);
 
    if (primero != "+" && primero != "-")
        return false;
 
    if (!parseInt(valor))
        return false;
 
    return true;
}


function nuevaFecha(fecha, intervalo) {
    var arrayFecha = fecha.split('-');
    var interv = intervalo.substring(1, intervalo.length);
    var operacion = intervalo.substring(0, 1);
    var dia = arrayFecha[2];
    var mes = arrayFecha[1];
    var anio = arrayFecha[0];
    var fechaInicial = new Date(anio, mes - 1, dia);
    var fechaFinal = fechaInicial;
     
    if (operacion == "+")
        fechaFinal.setDate(fechaInicial.getDate() + parseInt(intervalo));
    else
        fechaFinal.setDate(fechaInicial.getDate() - parseInt(intervalo));
     
    dia = fechaFinal.getDate();
    mes = fechaFinal.getMonth() + 1;
    anio = fechaFinal.getFullYear();
 
    dia = (dia.toString().length == 1) ? "0" + dia.toString() : dia;
    mes = (mes.toString().length == 1) ? "0" + mes.toString() : mes;
 
    return anio + "-" + mes + "-" + dia;
}