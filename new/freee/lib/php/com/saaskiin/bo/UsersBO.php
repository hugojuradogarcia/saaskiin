<?php

include_once $CFG->datarootDAOSaaskiin . 'UsersDAO.php';

/**
 * @author Enginner system Edwin michael
 * @category Web developer system
 * @copyright 2015
 *
 * Description UsersBO
 *
 */
class UsersBO {

    private $usersDAO;

    public function __construct() {
        $this->usersDAO = new UsersDAO();
    }

    public function __destruct() {
        unset($this->usersDAO);
    }

    private function validateArrayData() {
        return true;
    }

    public function getCountUsers($filter = null) {
        $count = $this->usersDAO->getCountUsersApp($filter);
        return $count;
    }

    public function getAllDataUsers($filter = null) {
        $resulset = null;
        $resulset = $this->usersDAO->getAllDataUsersApp($filter);
        return $resulset;
    }

    public function insertUsers($data) {
        $id = null;
        if ($this->validateArrayData()) {
            $id = $this->usersDAO->insertUsersApp($data);
        }
        return $id;
    }

    public function updateUsers($data) {
        $id = null;
        if ($this->validateArrayData()) {
            $id = $this->usersDAO->updateUsersApp($data);
        }
        return $id;
    }

}
?>




