        <header class="banner headerclass" role="banner">
            <section id="topbar" class="topclass no-mobile">
                <div class="container">
                    <div class="row">
                        <!--close col-md-6-->
                    </div>
                    <!-- Close Row -->
                </div>
                <!-- Close Container -->
            </section>
            <div class="container">
                <div id="mobile-nav-trigger" class="nav-trigger">
                    <a class="nav-trigger-case mobileclass collapsed" rel="nofollow" data-toggle="collapse" data-target=".kad-nav-collapse">
                        <div class="kad-navbtn">
                            <!--<i class="icon-reorder"></i>-->
                        </div>
                        <div class="kad-menu-name">Menu</div>
                    </a>
                </div>
                

                <div class="row">
                    <div class="col-md-12  clearfix kad-header-left">
                        <div id="logo" class="logocase">
                            <a class="brand logofont" href="index.php">
                                <div id="thelogo">
                                    <img src="img/logo.png" alt="SáasKíin" class="kad-standard-logo" />
                                </div>
                            </a>
                            <!--<p class="kad_tagline belowlogo-text">-->
                            <!--</p>-->
                        </div>
                        <!-- Close #logo -->
                        
                        <!-- Close span7 -->
                    </div>
                    <!-- Close Row -->
                </div>
            </div>
            

        </header>