        <footer id="containerfooter" class="footerclass" role="contentinfo">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 footercol1">
                        <div class="widget-1 widget-first footer-widget">
                            <aside id="nav_menu-5" class="widget widget_nav_menu">
                                <h3>Temas de Interés</h3>
                                <ul id="menu-helpful-links" class="menu">
                                    <li>
                                        <a href="faq.php" class="link">Preguntas Frecuentes</a>
                                    </li>
                                    <li>
                                        <a href="privacy.php" class="link">Aviso de Privacidad</a>
                                    </li>
                                    <li>
                                        <a href="who.php" class="link">Quienes Somos</a>
                                    </li>
                                    <li>
                                        <a href="policies.php" class="link">Políticas</a>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                    <div class="col-md-4 footercol2">
                        <div class="widget-1 widget-first footer-widget">
                            <aside id="text-4" class="widget widget_text">
                                <h3>Contacto</h3>
                                <div class="textwidget">
                                    <p>
                                        <a href="mailto:info@saaskiin.com">info@saaskiin.com</a>
                                    </p>
                                </div>
                            </aside>
                        </div>
                    </div>
                    <div class="col-md-4 footercol3">
                        <div class="widget-1 widget-first footer-widget">
                            <aside id="text-5" class="widget widget_text">
                                <h3>Social</h3>
                                <div class="textwidget">
                                    <a href="https://www.facebook.com/pages/Shampoo-anticaida-SAAS-KIIN/1650813605134295" target="_blank">
                                        <img width="63" height="63" title="Sáaskíin on Facebook" alt="saaskiin-facebook" src="http://saaskiin.com/img/facebook.png">
                                    </a>
                                    <a href="https://twitter.com/saaskiin" target="_blank">
                                        <img width="64" height="64" title="Sáaskíin on Twitter" alt="saaskiin-twitter" src="http://saaskiin.com/img/twitter.png">
                                    </a>
                                </div>
                            </aside>
                        </div>
                    </div>
                    <div class="footercredits clearfix">
                        <p>
                            © 2015 saaskiin.com. Todos los Derechos Reservados.
                        </p>
                    </div>
                </div>
            </div>
        </footer>