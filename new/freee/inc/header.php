        <header class="banner headerclass" role="banner">
            <section id="topbar" class="topclass no-mobile">
                <div class="container">
                    <div class="row">
                        <!--close col-md-6-->
                    </div>
                    <!-- Close Row -->
                </div>
                <!-- Close Container -->
            </section>
            <div class="container">
                <div id="mobile-nav-trigger" class="nav-trigger">
                    <a class="nav-trigger-case mobileclass collapsed" rel="nofollow" data-toggle="collapse" data-target=".kad-nav-collapse">
                        <div class="kad-navbtn">
                            <!--<i class="icon-reorder"></i>-->
                        </div>
                        <div class="kad-menu-name">Menu</div>
                    </a>
                </div>
                <div id="kad-mobile-nav" class="kad-mobile-nav">
                    <div class="kad-nav-inner mobileclass">
                        <div class="kad-nav-collapse">
                            <ul id="menu-mobile" class="kad-mnav">
                               <li><a href="store.php">Tienda</a></li>
                                <li><a href="why.php">¿Por qué Sáaskíin?</a></li>
                                <li><a href="contact.php">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12  clearfix kad-header-left">
                        <div id="logo" class="logocase">
                            <a class="brand logofont" href="index.php">
                                <div id="thelogo">
                                    <img src="img/logo.png" alt="SáasKíin" class="kad-standard-logo" />
                                </div>
                            </a>
                            <!--<p class="kad_tagline belowlogo-text">-->
                            <!--</p>-->
                        </div>
                        <!-- Close #logo -->
                        <!-- close logo span -->

                        <div class="col-md-12 kad-header-right">
                            <nav id="nav-main" class="clearfix" role="navigation">
                            </nav>
                        </div>
                        <!-- Close span7 -->
                    </div>
                    <!-- Close Row -->
                </div>
            </div>
            <!-- Close Container -->
            <section id="cat_nav" class="navclass">
                <div class="container">
                    <nav id="nav-second" class="clearfix" role="navigation">
                        <ul id="menu-main" class="sf-menu">
                            <li><a href="store.php" class="link">Tienda</a></li>
                            <li><a href="why.php" class="link">¿Por qué Sáaskíin?</a></li>
                            <li><a href="contact.php" class="link">Contacto</a></li>
                        </ul>
                    </nav>
                </div>
                <!--close container-->
            </section>

        </header>