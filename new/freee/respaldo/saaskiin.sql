/*
Navicat MySQL Data Transfer

Source Server         : conexion local
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : saaskiin

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-07-18 13:12:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `categoria_producto`
-- ----------------------------
DROP TABLE IF EXISTS `categoria_producto`;
CREATE TABLE `categoria_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) DEFAULT NULL,
  `abreviacion` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categoria_producto
-- ----------------------------
INSERT INTO `categoria_producto` VALUES ('1', 'shampoo', 'sh');
INSERT INTO `categoria_producto` VALUES ('2', 'crema reductora', 'cr');

-- ----------------------------
-- Table structure for `producto`
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_producto` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of producto
-- ----------------------------
INSERT INTO `producto` VALUES ('1', 'shampoo 1', 'caida prematura', '1');
INSERT INTO `producto` VALUES ('2', 'shampoo 2', 'caida media', '1');
INSERT INTO `producto` VALUES ('3', 'gel reductor de grasa', 'caida prolongada', '2');

-- ----------------------------
-- Table structure for `solicitudes`
-- ----------------------------
DROP TABLE IF EXISTS `solicitudes`;
CREATE TABLE `solicitudes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producto` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `mensaje` text,
  `creationdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of solicitudes
-- ----------------------------
INSERT INTO `solicitudes` VALUES ('3', '2', '3', 'hola soy un mensaje', '2015-07-18 02:57:43');
INSERT INTO `solicitudes` VALUES ('4', '1', '3', 'hola soy un mensaje', '2015-07-18 03:00:14');
INSERT INTO `solicitudes` VALUES ('5', '2', '4', 'sdfsdfsdfsdf', '2015-07-18 03:03:34');
INSERT INTO `solicitudes` VALUES ('6', '1', '5', 'dfgdfgdfgdfg', '2015-07-18 03:04:51');
INSERT INTO `solicitudes` VALUES ('7', '2', '6', 'xcvxcvxcvxcv', '2015-07-18 03:06:18');
INSERT INTO `solicitudes` VALUES ('8', '1', '7', null, '2015-07-18 03:12:29');
INSERT INTO `solicitudes` VALUES ('9', '1', '7', '', '2015-07-18 03:14:52');
INSERT INTO `solicitudes` VALUES ('10', '1', '7', null, '2015-07-18 03:15:24');
INSERT INTO `solicitudes` VALUES ('11', '1', '7', null, '2015-07-18 03:15:39');
INSERT INTO `solicitudes` VALUES ('12', '1', '7', null, '2015-07-18 03:16:03');
INSERT INTO `solicitudes` VALUES ('13', '1', '7', null, '2015-07-18 03:16:13');
INSERT INTO `solicitudes` VALUES ('14', '1', '7', null, '2015-07-18 03:16:43');
INSERT INTO `solicitudes` VALUES ('15', '2', '8', '', '2015-07-18 03:19:17');
INSERT INTO `solicitudes` VALUES ('16', '1', '9', '', '2015-07-18 03:21:48');
INSERT INTO `solicitudes` VALUES ('17', '1', '10', '', '2015-07-18 03:23:37');
INSERT INTO `solicitudes` VALUES ('18', '1', '11', '', '2015-07-18 03:28:06');
INSERT INTO `solicitudes` VALUES ('19', '2', '12', '', '2015-07-18 03:39:47');

-- ----------------------------
-- Table structure for `usuarios`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) DEFAULT NULL,
  `apellido_materno` varchar(150) DEFAULT NULL,
  `apellido_paterno` varchar(150) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `creationdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('3', 'edwin michael', '', '', 'michale870917@gmail.com', '5446456456', '', '2015-07-18 02:57:43');
INSERT INTO `usuarios` VALUES ('4', 'dasdasda', '', '', 'asdasdas@gmail.com', '4534534534', '', '2015-07-18 03:03:34');
INSERT INTO `usuarios` VALUES ('5', 'vvxcvxcv', '', '', 'xcvxcv@dfgdfg.com', '4564564564', '', '2015-07-18 03:04:51');
INSERT INTO `usuarios` VALUES ('6', 'dfgdfgd', '', '', 'dfgdfgdf@dfsdfs.com', '4456645645', '', '2015-07-18 03:06:18');
INSERT INTO `usuarios` VALUES ('7', 'fsdfsdfsdf', '', '', 'sdfsdfs@sdfsdf.com', '3534534534', '', '2015-07-18 03:12:29');
INSERT INTO `usuarios` VALUES ('8', 'fsdfsd', '', '', 'sdfsdfsdf@dsfsd.com', '5646456456', '', '2015-07-18 03:19:17');
INSERT INTO `usuarios` VALUES ('9', 'cvxvxcvx', '', '', 'xcvxcvxcv@sdfsdf.com', '5646645645', '', '2015-07-18 03:21:48');
INSERT INTO `usuarios` VALUES ('10', 'zxczxczx', '', '', 'zxczxczxc@sdfsdf.com', '4564564564', '', '2015-07-18 03:23:37');
INSERT INTO `usuarios` VALUES ('11', 'dfsdfsd', '', '', 'sdfsdfs@fsdfsdf.com', '4534534534', '', '2015-07-18 03:28:06');
INSERT INTO `usuarios` VALUES ('12', 'fsdfsdfsdfsdf', '', '', 'dfsdfsdfsdfs@fsfsd.com', '3534534534', '', '2015-07-18 03:39:47');
