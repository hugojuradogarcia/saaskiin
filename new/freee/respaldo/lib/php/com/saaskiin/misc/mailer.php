<?php
include_once 'class.phpmailer.php';
class mailer {

    var $host = "promociones1n.com";
    var $SMTPAuth = true;
    var $Username = "soporte@promociones1n.com";
    var $Password = 's0p0rt3$Pn1Pr0m0s';
    var $SMTPSecure = "tsl";
    var $CharSet = "utf-8";
    var $From = "soporte@promociones1n.com";
    var $FromName = "Promoción + de mil mexicanos a Brasil";
    var $IsHTML = true;
    var $ErrorInfo = "";
    var $Bcc = "soporte@promociones1n.com";
    var $mailObject;

    public function __construct() {
        $this->mailObject = new PHPMailer;
        $this->mailObject->IsSMTP();
        $this->mailObject->isHTML($this->IsHTML);
        $this->mailObject->Host = $this->host;
        $this->mailObject->SMTPAuth = $this->SMTPAuth;
        $this->mailObject->Username = $this->Username;
        $this->mailObject->Password = $this->Password;
        $this->mailObject->SMTPSecure = $this->SMTPSecure;
        $this->mailObject->CharSet = $this->CharSet;
        $this->mailObject->From = $this->From;
        $this->mailObject->FromName = $this->FromName;
        //$this->mailObject->addBCC($this->Bcc);
    }
    public function __destruct() {
        unset($this->mailObject);
    }
    
    public function send($to = "soporte@promociones1n.com", $toName="", $message="", $altMessage, $subject=""){
        $this->mailObject->addAddress($to, $toName);

        $this->mailObject->WordWrap = 50;
        $this->mailObject->isHTML(true);

        $this->mailObject->Subject = $subject;
        $this->mailObject->Body = $message;
        $this->mailObject->AltBody = $altMessage;

        if (!$this->mailObject->send()) {
            //echo 'Message could not be sent.';
            $this->ErrorInfo = $this->mailObject->ErrorInfo;
            return false;
        }
        return true;
    }

}

?>
