<?php

/**
 * Description of Abstract
 *
 * @author jopejimx
 */
class DaoAbstract {

    var $link;
    var $usuq;

    /** Short description 
     * Este método se ejecuta al inicio de cada instanciamiento y abre la conexion a la base de datos
     */
    public function __construct() {
        $dbConfig = new DBconfig();
        $this->link = mysqli_init();
        mysqli_real_connect($this->link, $dbConfig->server, $dbConfig->username, $dbConfig->password, $dbConfig->databasename, $dbConfig->port);
        mysqli_set_charset($this->link, "utf8");
        $this->throwExceptionOnError($this->link);
    }

    /** Short Description
     * Este método se ejecuta al finalizar el ciclo de vida del objeto cerrando la conexion a la base de datos
     * 
     */
    public function __destruct() {
        mysqli_close($this->link);
    }

    /**
     * Función publica que permite conectar a la base de datos y recuperar registros
     * a partir de una consulta
     *
     * @param mysqli $link Link de conexion mysqli
     * @param String $query Cadena con la consulta a ejecutar
     * @param array $fieldNames array con los nombre de los campos a consultar
     * @param array $fieldTitles array con los nombres de los campos a regresar o nulo si se utiliza el de la base de datos
     * o null si no tiene
     */
    public function getBySqlQuery(&$link, $query, $fieldNames, $fieldTitles = null) {
        $sqlStatement = $query;
        $stmt = mysqli_prepare($link, $sqlStatement);
        $this->throwExceptionOnError();
        mysqli_stmt_execute($stmt);
        $this->throwExceptionOnError();
        $rows = array();
        $strBinResults = "mysqli_stmt_bind_result(\$stmt";
        if ($fieldTitles == null) {
            foreach ($fieldNames as $field) {
                $strBinResults .= ", \$row->" . $field;
            }
        } else {
            foreach ($fieldTitles as $field) {
                $strBinResults .= ", \$row->" . $field;
            }
        }
        $strBinResults .= ");";
        eval($strBinResults);
        $this->throwExceptionOnError();
        while (mysqli_stmt_fetch($stmt)) {
            $rows[] = $row;
            $row = new stdClass();
            eval($strBinResults);
        }
        mysqli_stmt_free_result($stmt);

        if (count($rows) > 0) {
            return $rows;
        } else {
            return null;
        }
    }

    /**
     * Función publica que permite conectar a la base de datos e inserta
     * un nuevo registro en una tabla especifica
     *
     * @param mysqli $link Link de conexion mysqli
     * @param String $table Cadena con el nombre de la tabla a consultar
     * @param array $fieldList array con los nombre de los campos a consultar
     * @param String $bindTypes Cadena con los tipos de datos a enviar
     * al stmt o null si no tiene
     * @param array $bindValues Arreglo con los valores a bin al stmt
     * o null si no tiene
     * @return int $autoid Entero con el idenficador del nuevo registro
     */
    public function insert(&$link, $table, $fieldList, $bindTypes, $bindValues) {
        $sqlStatement = "INSERT INTO $table (" . implode(",", $fieldList) . ") VALUES(";
        for ($i = 0; $i < count($fieldList); $i++) {
            if ($i > 0) {
                $sqlStatement .= ",";
            }
            $sqlStatement .= "?";
        }
        $sqlStatement .=")";
        $stmt = mysqli_prepare($link, $sqlStatement);
        $this->throwExceptionOnError();
        if ($bindTypes != null && $bindValues != null) {
            $strBinParams = "mysqli_stmt_bind_param(\$stmt, \$bindTypes";
            for ($i = 0; $i < count($bindValues); $i++) {
                $strBinParams .= ", \$bindValues[" . $i . "]";
            }
            $strBinParams .= ");";
            eval($strBinParams);
            $this->throwExceptionOnError();
        }
        mysqli_stmt_execute($stmt);
        $this->throwExceptionOnError();
        $autoid = mysqli_stmt_insert_id($stmt);
        mysqli_stmt_free_result($stmt);
        return $autoid;
    }

    /**
     * Función publica que permite conectar a la base de datos y actualizar
     * un registro de una tabla especifica
     *
     * @param mysqli $link Link de conexion mysqli
     * @param String $table Cadena con el nombre de la tabla a consultar
     * @param array $fieldList array con los nombre de los campos a consultar
     * @param String $bindTypes Cadena con los tipos de datos a enviar
     * al stmt o null si no tiene
     * @param array $bindValues Arreglo con los valores a bin al stmt
     * o null si no tiene
     */
    public function update(&$link, $table, $fieldList, $fieldIdList, $bindTypes, $bindValues) {
        $sqlStatement = "UPDATE $table SET ";
        for ($i = 0; $i < count($fieldList); $i++) {
            if ($i > 0) {
                $sqlStatement .= ",";
            }
            $sqlStatement .= $fieldList[$i] . " = ?";
        }
        $sqlStatement .=" WHERE ";
        for ($i = 0; $i < count($fieldIdList); $i++) {
            if ($i > 0) {
                $sqlStatement .= " AND ";
            }
            $sqlStatement .= $fieldIdList[$i] . " = ?";
        }

        //echo $sqlStatement.' query';

        $stmt = mysqli_prepare($link, $sqlStatement);
        $this->throwExceptionOnError();
        if ($bindTypes != null && $bindValues != null) {
            $strBinParams = "mysqli_stmt_bind_param(\$stmt, \$bindTypes";
            for ($i = 0; $i < count($bindValues); $i++) {
                $strBinParams .= ", \$bindValues[" . $i . "]";
            }
            $strBinParams .= ");";
            eval($strBinParams);
            $this->throwExceptionOnError();
        }
        mysqli_stmt_execute($stmt);
        $this->throwExceptionOnError();
        mysqli_stmt_free_result($stmt);
    }

    /**
     * Función publica que permite conectar a la base de datos y eliminar
     * un registro de una tabla especifica
     *
     * @param mysqli $link Link de conexion mysqli
     * @param String $table Cadena con el nombre de la tabla a consultar
     * @param array $fieldList array con los nombre de los campos a consultar
     * @param String $bindTypes Cadena con los tipos de datos a enviar
     * al stmt o null si no tiene
     * @param array $bindValues Arreglo con los valores a bin al stmt
     * o null si no tiene
     */
    public function delete(&$link, $table, $fieldIdList, $bindTypes, $bindValues) {
        $sqlStatement = "DELETE FROM $table WHERE ";
        for ($i = 0; $i < count($fieldIdList); $i++) {
            if ($i > 0) {
                $sqlStatement .= " AND ";
            }
            $sqlStatement .= $fieldIdList[$i] . " = ?";
        }
        $stmt = mysqli_prepare($link, $sqlStatement);
        //echo $sqlStatement;
        $this->throwExceptionOnError();
        if ($bindTypes != null && $bindValues != null) {
            $strBinParams = "mysqli_stmt_bind_param(\$stmt, \$bindTypes";
            for ($i = 0; $i < count($bindValues); $i++) {
                $strBinParams .= ", \$bindValues[" . $i . "]";
            }
            $strBinParams .= ");";
            eval($strBinParams);
            $this->throwExceptionOnError();
        }
        mysqli_stmt_execute($stmt);
        $this->throwExceptionOnError();
        mysqli_stmt_free_result($stmt);
    }

    /**
     * Utilidad para lanzar una exception si ocurre un error
     * durante la ejecución de un comando de mysql
     */
    public function throwExceptionOnError($link = null) {
        if ($link == null) {
            $link = $this->link;
        }
        if (mysqli_error($link)) {
            $msg = mysqli_errno($link) . ": " . mysqli_error($link);
            throw new Exception('MySQL Error - ' . $msg);
        }

        
    }

}

?>