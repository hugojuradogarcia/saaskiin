<?php

include_once 'DBConfig.php';
include_once 'DaoAbstract.php';

/**
 *
 * @author Enginner system Edwin michael
 * @category Web developer system
 * @copyright 2015
 *
 *
 */
class SolicitudesDAO extends DaoAbstract {

    private $_basicFieldNames;
    private $_tableName;
    private $_bindTypes;

    public function __construct() {
        $this->_basicFieldNames = array("id","id_producto","id_usuario","mensaje","creationdate");
        $this->_tableName = "solicitudes";
        $this->_bindTypes = "iiiss";
        parent::__construct();
    }

    public function __destruct() {
        unset($this->_basicFieldNames);
        unset($this->_tableName);
        unset($this->_bindTypes);
        parent::__destruct();
    }

    public function getCountSolicitudesApp($filter = null) {
        $count = 0;
        $resulset = null;
        $query = "select count(*) as total from $this->_tableName" .
                ($filter != null ? " WHERE " . $filter : "");
        $resulset = $this->getBySqlQuery($this->link, $query, array("total"));
        if ($resulset !== null && count($resulset) > 0)
            $count = $resulset[0]->total;
        return $count;
    }

    public function getAllDataSolicitudesApp($filter = null) {
        $resulset = null;
        $query = "SELECT " . implode(",", $this->_basicFieldNames) . "
                  FROM
                  $this->_tableName
                   " .
                ($filter != null ? " WHERE " . $filter : "");
        $resulset = $this->getBySqlQuery($this->link, $query, $this->_basicFieldNames);
        return $resulset;
    }

    public function insertSolicitudesApp($data) {
        $id = 0;
        $insertField = $this->_basicFieldNames;
        array_shift($insertField);
        $id = $this->insert($this->link, $this->_tableName, $insertField, substr($this->_bindTypes, 1), $data);
        return $id;
    }

    

}

?>