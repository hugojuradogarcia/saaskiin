<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>SáasKíin</title>
    <link href="http://saaskiin.com/img/favicon.png" type="image/png" rel="icon">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,900,400italic,700italic,900italic">
    <link id="jquery.fancybox-css" media="all" type="text/css" href="css/jquery.fancybox.css" rel="stylesheet">
    <link id="kadence_bootstrap-css" media="all" type="text/css" href="css/bootstrap.css" rel="stylesheet">
    <link id="kadence_theme-css" media="all" type="text/css" href="css/virtue.css" rel="stylesheet">
    <link id="virtue_skin-css" media="all" type="text/css" href="css/default.css" rel="stylesheet">
    <link id="roots_child-css" media="all" type="text/css" href="css/style.css" rel="stylesheet">
    <link id="redux-google-fonts-css" media="all" type="text/css" href="https://fonts.googleapis.com/css?family=Pacifico%3A400%7CLato%3A400%2C700%7CGeorgia%3A700%2C400&subset=latin&ver=1422151648" rel="stylesheet">
    <!--<link rel="stylesheet" href="css/autoptimize_74cf5543c20f7104268b898327233b01.css" media="all" type="text/css">-->

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="http://saaskiin.com/js/autoptimize_2224ae0bd693af2c10779433ca32554a.js" defer="" type="text/javascript"></script>

</head>

<body>
    <div id="wrapper" class="container">

        <header class="banner headerclass" role="banner">
            <section id="topbar" class="topclass no-mobile">
                <div class="container">
                    <div class="row">
                        <!--close col-md-6-->
                    </div>
                    <!-- Close Row -->
                </div>
                <!-- Close Container -->
            </section>
            <div class="container">
                <div id="mobile-nav-trigger" class="nav-trigger">
                    <a class="nav-trigger-case mobileclass collapsed" rel="nofollow" data-toggle="collapse" data-target=".kad-nav-collapse">
                        <div class="kad-navbtn">
                            <!--<i class="icon-reorder"></i>-->
                        </div>
                        <div class="kad-menu-name">Menu</div>
                    </a>
                </div>
                <div id="kad-mobile-nav" class="kad-mobile-nav">
                    <div class="kad-nav-inner mobileclass">
                        <div class="kad-nav-collapse">
                            <ul id="menu-mobile" class="kad-mnav">
                                <li><a href="http://saaskiin.com/store.html">Tienda</a></li>
                                <li><a href="http://saaskiin.com/why.html">¿Por qué Sáaskíin?</a></li>
                                <li><a href="http://saaskiin.com/contact.html">Contacto</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12  clearfix kad-header-left">
                        <div id="logo" class="logocase">
                            <a class="brand logofont" href="http://saaskiin.com/">
                                <div id="thelogo">
                                    <img src="img/logo.png" alt="SáasKíin" class="kad-standard-logo" />
                                </div>
                            </a>
                            <!--<p class="kad_tagline belowlogo-text">-->
                            <!--</p>-->
                        </div>
                        <!-- Close #logo -->
                        <!-- close logo span -->

                        <div class="col-md-12 kad-header-right">
                            <nav id="nav-main" class="clearfix" role="navigation">
                            </nav>
                        </div>
                        <!-- Close span7 -->
                    </div>
                    <!-- Close Row -->
                </div>
            </div>
            <!-- Close Container -->
            <section id="cat_nav" class="navclass">
                <div class="container">
                    <nav id="nav-second" class="clearfix" role="navigation">
                        <ul id="menu-main" class="sf-menu">
                            <li><a href="http://saaskiin.com/store.html">Tienda</a></li>
                            <li><a href="http://saaskiin.com/why.html">¿Por qué Sáaskíin?</a></li>
                            <li><a href="http://saaskiin.com/contact.html">Contacto</a></li>
                        </ul>
                    </nav>
                </div>
                <!--close container-->
            </section>

        </header>

        <div class="wrap contentclass" role="document">
            <div id="pageheader" class="titleclass">
                <div class="container"></div>
            </div>
            <div id="content" class="container">
                <div class="row">
                    <div class="main col-md-12" role="main">
                        <h1>POLÍTICAS</h1>
                        <br />
                        <p>1.- Selecciona el producto de tu agrado. indica la cantidad de artículos. Nota: Por el momento no hacemos cobros en línea.</p>
                        <p>2.- Realiza tu pedido llenando el formulario de contacto e indica la cantidad y producto elegido. </p>
                        <p>3.- En las siguientes 48 hrs habiles recibirás un mail con la confirmación de la recepción de tu pedido, así como el número de cuenta para tu depósito o transferencia. Ahí encontrarás EL total a pagar, más gastos de envío. </p>
                        <p>4.- Realiza tu depósito o transferencia en la cuenta indicada en el último correo, en un máximo de 48horas. Envíanos el comprobante de tu pago a
                            <em>
                                <a href="pagos@saaskiin.com">pagos@saaskiin.com</a>
                            </em> 
                        </p>
                        <p>5.- Recibirás un mail con la confirmación de tu depósito y a partir de este momento correrán de 8 a 10días hábiles de entrega en tu domicilio.</p>
                        <br />
                        <p>** Todo pedido necesita una confirmacion de recibido.</p>
                        <p>** Conserva tu número de pedido, será necesario para cualquier comentario, duda o aclaración.</p>
                    </div>
                </div>
            </div>
        </div>

        <footer id="containerfooter" class="footerclass" role="contentinfo">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 footercol1">
                        <div class="widget-1 widget-first footer-widget">
                            <aside id="nav_menu-5" class="widget widget_nav_menu">
                                <h3>Temas de Interés</h3>
                                <ul id="menu-helpful-links" class="menu">
                                    <li>
                                        <a href="http://saaskiin.com/faq.html">Preguntas Frecuentes</a>
                                    </li>
                                    <li>
                                        <a href="http://saaskiin.com/privacy.html">Aviso de Privacidad</a>
                                    </li>
                                    <li>
                                        <a href="http://saaskiin.com/who.html">Quienes Somos</a>
                                    </li>
                                    <li>
                                        <a href="http://saaskiin.com/policies.html">Políticas</a>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                    <div class="col-md-4 footercol2">
                        <div class="widget-1 widget-first footer-widget">
                            <aside id="text-4" class="widget widget_text">
                                <h3>Contacto</h3>
                                <div class="textwidget">
                                    <p>
                                        <a href="mailto:info@saaskiin.com">info@saaskiin.com</a>
                                    </p>
                                </div>
                            </aside>
                        </div>
                    </div>
                    <div class="col-md-4 footercol3">
                        <div class="widget-1 widget-first footer-widget">
                            <aside id="text-5" class="widget widget_text">
                                <h3>Social</h3>
                                <div class="textwidget">
                                    <a href="https://www.facebook.com/pages/Shampoo-anticaida-SAAS-KIIN/1650813605134295" target="_blank">
                                        <img width="63" height="63" title="Sáaskíin on Facebook" alt="saaskiin-facebook" src="http://saaskiin.com/img/facebook.png">
                                    </a>
                                    <a href="https://twitter.com/saaskiin" target="_blank">
                                        <img width="64" height="64" title="Sáaskíin on Twitter" alt="saaskiin-twitter" src="http://saaskiin.com/img/twitter.png">
                                    </a>
                                </div>
                            </aside>
                        </div>
                    </div>
                    <div class="footercredits clearfix">
                        <p>
                            © 2015 saaskiin.com. Todos los Derechos Reservados.
                        </p>
                    </div>
                </div>
            </div>
        </footer>

    </div>
</body>

</html>
