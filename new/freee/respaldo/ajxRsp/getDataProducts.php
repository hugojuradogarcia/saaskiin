<?php
    if (!isset($CFG))
        include_once '../config.php';
        include_once $CFG->datarootUti;
        include_once $CFG->datarootBOSaaskiin.'ProductsBO.php';
    date_default_timezone_set("America/Mexico_City");
    $utilities = new Utilities();
    $products = new ProductsBO();
    
    $response = new stdClass();
    $array = array();
    $dataInfo = $products->getAllDataProducts();
    if(count($dataInfo) > 0){
        foreach($dataInfo AS $viewInfo){ 
             $array[] = array("keyProduct" => $viewInfo->id, "nameProduct" => $viewInfo->nombre_producto);                     
        }
        $response->build = $array;
        $response->result = true;
    }else{
        $response->result = false;
    }
    echo json_encode($response);
?>
