<?php  
unset($CFG);
global $CFG;
global $dirComplete;
$CFG = new stdClass();
$CFG->dataroot  = dirname(__FILE__);
$CFG->datarootBOSaaskiin  = dirname(__FILE__)."/lib/php/com/saaskiin/bo/";
$CFG->datarootDAOSaaskiin  = dirname(__FILE__)."/lib/php/com/saaskiin/dao/";
$CFG->datarootUti  = dirname(__FILE__)."/lib/php/application/utilities.php";

/**
 * Clase generica stdClass
 * para colocar cualquier dato en el momento en que lo necesitemos
 * sin necesidad de declarar una clase
 * 
 */ 
?>
