<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saaskiin</title>
    <?php include_once("includes/styles.php"); ?>
</head>
<body>
    <?php include_once("includes/headerinit.php"); ?>

    <section class="container">
        <div class="row">
            <div class="why col-md-12 text-center">
                <h1>TEMAS DE INTERÉS</h1>
            </div>
            <div class="clearfix"></div>
            <div class="margintop100"></div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h2>¿Quiénes pueden usar el Shampoo Anticaida?</h2>
                <h3>Hombres y mujeres de todas las edades, el shampoo fue creado prevenir la caía del cabello y es para toda la familia.</h3>
            </div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h2>¿Cómo usar el Shampoo Anticaida?</h2>
                <h3>Mojar el cabello, aplicar cantidad suficiente dando masaje suavemente en el cuero cabelludo y dejar actuar por 3-5 minutos, enjuagar con abundante agua.</h3>
            </div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h2>¿Puedo usar acondicionador?</h2>
                <h3>Sí, aplica tu acondicionador de preferencia solo en las puntas de tu cabello.</h3>
            </div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h2>¿Cómo debo secar mi cabello?</h2>
                <h3>Envuélvelo en una toalla y déjalo secar en ella durante 20 minutos. Cuando retires la toalla el pelo ya estará prácticamente seco.</h3>
            </div>
    <div class="clearfix"></div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h2>¿Cuánto tiempo debo usar Saas Kíin?</h2>
                <h3>El uso frecuente del Shampoo Anticaida fortalecerá tu cabello de manera permanente, devolviéndole su salud y brillo natural.</h3>
            </div>




            <div class="col-md-12 bajito"></div>
        </div>
    </section>
    <?php include_once("includes/footer.php"); ?>

    
</body>

<script src="lib/js/vendor/jquery-1.11.3.min.js"></script>
<script src="lib/js/vendor/bootstrap.min.js"></script>
</html>