<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saaskiin</title>
    <?php include_once("includes/styles.php"); ?>
</head>
<body>
    <?php include_once("includes/headerinit.php"); ?>

    <section class="container">
        <div class="row">
            <div class="why col-md-12 text-center">
                <h1>¿QUIENES SOMOS?</h1>
            </div>
            <div class="clearfix"></div>
            <div class="margintop100"></div>
            <div class="why col-md-12">
                <span>
                    <div class="col-md-11">
                        <p> Saas Kiin es una empresa mexicana enfocada en productos que previenen la caída del cabello, pensando en toda la familia.</p>
                    </div>
                </span>
                <div class="paddingbottom70"></div>
                <div class="clearfix"></div>
                <span>
                    <div class="col-md-11">
                        <p> Está comprobado que el estrés que hoy afecta a toda la población siendo una causa importante de la perdida de cabello prematuramente.</p>
                    </div>
                    
                </span>
                <div class="paddingbottom70"></div>
                <div class="clearfix"></div>
                <span>
                    <div class="col-md-11">
                        <p> Saas Kiin pone a tu alcance el Shampoo Anticaida que te ayudará a fortalecerlo desde la raíz devolviendo su brillo y salud.</p>
                    </div>
                  
                </span>
            </div>

            <div class="col-md-12 bajito"></div>
        </div>
    </section>
    <?php include_once("includes/footer.php"); ?>

    
</body>

<script src="lib/js/vendor/jquery-1.11.3.min.js"></script>
<script src="lib/js/vendor/bootstrap.min.js"></script>
</html>