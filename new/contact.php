<?php
if (!isset($CFG))
        include_once 'config.php';
        include_once $CFG->datarootUti;
        include_once $CFG->datarootBOSaaskiin.'ProductsBO.php';
    date_default_timezone_set("America/Mexico_City");
    $utilities = new Utilities();
    $products = new ProductsBO();
    
    $response = new stdClass();
    $array = array();
    $dataInfo = $products->getAllDataProducts();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saaskiin</title>
    <?php include_once("includes/styles.php"); ?>

</head>
<body>
    <?php include_once("includes/headerinit.php"); ?>

    <section id="contact" class="container" data-ng-app="ControllerMain">
        <div class="row">
            <div class="relcontact col-md-7">
                <h1>Contacto</h1>
                <div class="col-md-10" data-ng-controller="ControllerForm">
                    <form name="FormControllerAction" id="FormControllerAction"  role="form" novalidate>
                        <div class="form-group">
                            <small class="errorSpan" data-ng-show="FormControllerAction.namecontact.$dirty && FormControllerAction.namecontact.$error.required || submitted && FormControllerAction.namecontact.$error.required">El nombre es requerido.</small>
                            <small class="errorSpan" data-ng-show="FormControllerAction.namecontact.$error.pattern">El nombre debe ser mayor a 3 digitos y solo debe contener letras.</small>
                            <input type="text" class="form-control" autocomplete="off" placeholder='Nombre' name='namecontact' data-ng-model='formData.namecontact' required ng-pattern="soloLetras" data-ng-class='{ error: FormControllerAction.namecontact.$invalid && FormControllerAction.namecontact.$dirty, warning: FormControllerAction.namecontact.$error.namecontact , success: FormControllerAction.namecontact.$valid }'>
                        </div>

                        <div class="form-group">
                            <span class="errorSpan" data-ng-show='FormControllerAction.email.$dirty && FormControllerAction.email.$error.required || submitted && FormControllerAction.email.$error.required'>El email es requerido</span>
                            <span class="errorSpan" data-ng-show='FormControllerAction.email.$error.pattern'>Por favor coloca un email correcto.</span>
                            <input type="text" class="form-control" autocomplete="off" placeholder='Correo electronico' name='email' data-ng-model='formData.email' required ng-pattern="correovalido" data-ng-class='{ error: FormControllerAction.email.$invalid && FormControllerAction.email.$dirty, warning: FormControllerAction.email.$error.email, success: FormControllerAction.email.$valid }'>
                        </div>

                        <div class="form-group">
                            <span class="errorSpan" data-ng-show="FormControllerAction.product.$dirty && FormControllerAction.product.$error.required || submitted && FormControllerAction.product.$error.required">El producto es requerido.</span>
                            <select id="product" class="form-control" name="product" ng-model="formData.product"  required data-ng-class='{ error: FormControllerAction.product.$invalid && FormControllerAction.product.$dirty , success: FormControllerAction.product.$valid }'>
                                    <option value="">Selecciona un producto</option>
                                    <?php
                                        if(count($dataInfo) > 0){
                                            foreach($dataInfo AS $viewInfo){
                                                echo "<option value='$viewInfo->id'>$viewInfo->nombre_producto</option>";                     
                                            }
                                        }
                                    ?>
                            </select>  
                        </div>

                         <div class="form-group">
                            <span class="errorSpan" data-ng-show='FormControllerAction.telefono.$dirty && FormControllerAction.telefono.$error.required || submitted && FormControllerAction.telefono.$error.required'>El teléfono es requerido</span>
                            <span class="errorSpan" data-ng-show='FormControllerAction.telefono.$error.pattern'>Por favor coloca un teléfono correcto.</span>
                            <input type="text" class="form-control" maxlength="10" required placeholder='Teléfono' name='telefono' id="telefono" data-ng-model='formData.telefono' ng-pattern="dieznumeros" data-ng-class="{ error: FormControllerAction.telefono.$invalid && FormControllerAction.telefono.$dirty , success: FormControllerAction.telefono.$valid }">
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" name="mensaje" rows="4" id="mensaje" placeholder="mensaje" tabindex="6" data-ng-model='formData.mensaje'></textarea>
                        </div>

                        <div class="form-group">
                            <div class="captcha-resultado">
                                <span class="errorSpan" data-ng-show="FormControllerAction.resultado.$dirty && FormControllerAction.resultado.$error.required || submitted && FormControllerAction.resultado.$error.required">Debes colocar un resultado.</span>
                                <span class="errorSpan" data-ng-show=''>Por favor colocar el resultado correcto.</span>
                                <div class="col-md-6">
                                    <span class="newsize">Demuestra que no eres un robot</span>
                                    <div class="lineachi">
                                        <captcha field1="{{field1}}" operator="{{operator}}" field2="{{field2}}"></captcha>
                                    </div>    
                                </div>

                                <div class="col-md-6">
                                    <div>
                                        <input class="form-control" name="resultado" id="resultado" data-ng-model="formData.resultado" data-wc-unique="formData.resultado" required />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button id="yesyes" class="form-control btn btn-block botoncontact" data-ng-click="sendDataForm(FormControllerAction)">Enviar</button>
                        </div>
                      </form>          
                </div>

            </div>
            <div class="col-md-5">
                <img class="img-circle" alt="140x140" src="img/contact.png" />
            </div>
        </div>
    </section>

    <?php include_once("includes/footer.php"); ?>

    
</body>

<script src="lib/js/vendor/jquery-1.11.3.min.js"></script>
<script src="lib/js/vendor/bootstrap.min.js"></script>
<script src="lib/js/vendor/angular.min.js" type="text/javascript"></script>
<script src="lib/js/custom/controllerforms.js" type="text/javascript"></script>
<script src="lib/js/custom/functions-reusable.js" type="text/javascript"></script>
<script src="lib/js/custom/captcha.js" type="text/javascript"></script>
<script src="lib/js/custom/muestreo.js" type="text/javascript"></script>
</html>