<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saaskiin</title>
    <?php include_once("includes/styles.php"); ?>
</head>
<body>
    <?php include_once("includes/headerinit.php"); ?>

    <section class="container">
        <div class="row">
            <div class="why col-md-12 text-center">
                <h1>POLÍTICAS</h1>
            </div>
            <div class="clearfix"></div>
            <div class="margintop100"></div>
            <div class="why col-md-12">
                <h2 class="policies">1.- Selecciona el producto de tu agrado. indica la cantidad de artículos. Nota: Por el momento no hacemos cobros en línea.</h2>
                
                <h2 class="policies">2.- Realiza tu pedido llenando el formulario de contacto e indica la cantidad y producto elegido.</h2>
                
                <h2 class="policies">3.- En las siguientes 48 hrs habiles recibirás un mail con la confirmación de la recepción de tu pedido, así como el número de cuenta para tu depósito o transferencia. Ahí encontrarás EL total a pagar, más gastos de envío.</h2>
                
                <h2 class="policies">4.- Realiza tu depósito o transferencia en la cuenta indicada en el último correo, en un máximo de 48horas. Envíanos el comprobante de tu pago a <a href="pagos@saaskiin.com">pagos@saaskiin.com</a></h2>
          
                <h2 class="policies">5.- Recibirás un mail con la confirmación de tu depósito y a partir de este momento correrán de 8 a 10días hábiles de entrega en tu domicilio.</h2>

                <h3>** Todo pedido necesita una confirmacion de recibido.</h3>
                <h3>** Conserva tu número de pedido, será necesario para cualquier comentario, duda o aclaración.</h3>
            </div>

            <div class="col-md-12 bajito"></div>
        </div>
    </section>
    <?php include_once("includes/footer.php"); ?>

    
</body>

<script src="lib/js/vendor/jquery-1.11.3.min.js"></script>
<script src="lib/js/vendor/bootstrap.min.js"></script>
</html>