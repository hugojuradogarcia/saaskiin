<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Saaskiin</title>
    <?php include_once("includes/styles.php"); ?>
</head>
<body>
    <?php include_once("includes/headerinit.php"); ?>

    <section class="container">
        <div class="row">
            <div class="why col-md-12 text-center">
                <h1>¿POR QUE SAAS KIIN?</h1>
            </div>
            <div class="clearfix"></div>
            <div class="margintop100"></div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h3>1.- Ayuda al fortalecimiento desde la raíz hasta las puntas previniendo su debilitamiento y la caída prematura.</h3>
            </div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h3>2.- Es tan suave con tu cabello que podrás usarlo diariamente.</h3>
            </div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h3>3.- Estimula el crecimiento de cabello.</h3>
            </div>
            <div class="why col-md-3">
                <div class="clearfix"></div>
                <h3>4.- Cambios visibles en 3 meses de uso.</h3>
            </div>

            <div class="col-md-12 bajito"></div>
        </div>
    </section>
    <?php include_once("includes/footer.php"); ?>

    
</body>

<script src="lib/js/vendor/jquery-1.11.3.min.js"></script>
<script src="lib/js/vendor/bootstrap.min.js"></script>
</html>