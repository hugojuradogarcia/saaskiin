<?php

include_once 'DBConfig.php';
include_once 'DaoAbstract.php';

/**
 *
 * @author Enginner system Edwin michael
 * @category Web developer system
 * @copyright 2015
 *
 *
 */
class UsersDAO extends DaoAbstract {

    private $_basicFieldNames;
    private $_tableName;
    private $_bindTypes;

    public function __construct() {
        $this->_basicFieldNames = array("id","nombre","apellido_materno","apellido_paterno","email","telefono","celular","creationdate");
        $this->_tableName = "usuarios";
        $this->_bindTypes = "isssssss";
        parent::__construct();
    }

    public function __destruct() {
        unset($this->_basicFieldNames);
        unset($this->_tableName);
        unset($this->_bindTypes);
        parent::__destruct();
    }

    public function getCountUsersApp($filter = null) {
        $count = 0;
        $resulset = null;
        $query = "select count(*) as total from $this->_tableName" .
                ($filter != null ? " WHERE " . $filter : "");
        $resulset = $this->getBySqlQuery($this->link, $query, array("total"));
        if ($resulset !== null && count($resulset) > 0)
            $count = $resulset[0]->total;
        return $count;
    }

    public function getAllDataUsersApp($filter = null) {
        $resulset = null;
        $query = "SELECT " . implode(",", $this->_basicFieldNames) . "
                  FROM
                  $this->_tableName
                   " .
                ($filter != null ? " WHERE " . $filter : "");
        $resulset = $this->getBySqlQuery($this->link, $query, $this->_basicFieldNames);
        return $resulset;
    }

    public function insertUsersApp($data) {
        $id = 0;
        $insertField = $this->_basicFieldNames;
        array_shift($insertField);
        $id = $this->insert($this->link, $this->_tableName, $insertField, substr($this->_bindTypes, 1), $data);
        return $id;
    }

    public function updateUsersApp($data) {
        $id = 0;
        $campos = array("nombre","apellido_materno","apellido_paterno","email","telefono","celular");
        $clave = array('id');
        $id = $this->update($this->link, $this->_tableName, $campos, $clave, 'ssssssi', $data);
        if ($data[6]) {
            $id = $data[6];
        }
        return $id;
    }

}

?>